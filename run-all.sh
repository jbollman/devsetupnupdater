#!/bin/bash

if [ -e "/etc/fedora-release" ]
then
  echo "rhel-based-os"
  ./config-files.sh
  ./flatpak-config.sh
  ./dnf-update-upgrade.sh

  ./dnf-packages-install.sh
  ./flatpak-list-install.sh
  ./ide-setup.sh
else
  echo "apt-based-os"
  ./config-files.sh

  ./update-upgrade.sh

  ./apt-packages-install.sh

  ./snap-list-install.sh

fi
