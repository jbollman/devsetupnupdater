#!/bin/bash

dnf install go -y
dnf install vim -y

#sudo dnf install snapd -y
sudo dnf install java-1.8.0-openjdk

dnf install neovim -y

dnf install neofetch -y

sudo dnf install vagrant virtualbox -y

sudo dnf install htop wget git unzip curl tree -y

sudo dnf install pgadmin3 -y
