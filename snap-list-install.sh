#!/bin/bash

sudo snap install docker

sudo snap install go --classic

sudo snap install goland --classic

sudo snap install sublime-text --classic

sudo snap install intellij-idea-community --classic

sudo snap install android-studio --classic

sudo snap install pycharm-community --classic

sudo snap install eclipse --classic

sudo snap install clion --classic

sudo snap install tusk

sudo snap install arduino-mhall119
