#!/bin/bash

mkdir -p ~/bin/

export PATH=$PATH:~/bin


cd ~/bin

if [ ! -e "./ideaIC"* ]
then
  wget --no-check-certificate https://download.jetbrains.com/idea/ideaIC-2018.2.5.tar.gz; tar -xvf id*;
else
 echo "java ide tar.gz already downloaded"
fi

if [ ! -e "./CL"* ]
then
  wget --no-check-certificate https://download.jetbrains.com/cpp/CLion-2018.2.5.tar.gz; tar -xvf CL*;
else
  echo "CLION alread downloaded"
fi

if [ ! -e "./go"* ]
then
  wget -- no-check-certificate https://download.jetbrains.com/go/goland-2018.2.3.tar.gz; tar -xvf go*;
else
  echo "Goland ide already downloaded"
fi

sudo dnf install codeblocks anjuta -y

